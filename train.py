import tensorflow as tf
# import keras
from tensorflow import keras

import numpy as np

minst = keras.datasets.mnist
(trax, tray), (tex, tey) = minst.load_data()


trax = np.array(trax / 255)
tex = np.array(tex / 255)
print(trax.shape)
print(tray.shape)

trax = np.expand_dims(trax, axis=3)
tex = np.expand_dims(tex, axis=3)

# model = keras.models.Sequential([
#     keras.layers.Conv2D(32, (5, 5), padding='same', input_shape=(28, 28, 1)),
#     keras.layers.LeakyReLU(alpha=0.02),
#     keras.layers.MaxPool2D(pool_size=(2, 2)),
#     keras.layers.Dropout(0.2),

#     keras.layers.Conv2D(64, (5, 5)),
#     keras.layers.LeakyReLU(alpha=0.02),
#     keras.layers.MaxPool2D(pool_size=(2, 2)),
#     keras.layers.Dropout(0.2),

#     keras.layers.GlobalMaxPool2D(),

#     keras.layers.Dense(512),
#     keras.layers.LeakyReLU(0.2),
#     keras.layers.Dropout(0.4),

#     keras.layers.Dense(10, activation='softmax')
# ])

inputs = keras.layers.Input(shape=(28, 28, 1))

conv1   = keras.layers.Conv2D(32, (5, 5), padding='same', strides=(1, 1))(inputs)
relu1   = keras.layers.LeakyReLU(alpha=0.02)(conv1)
pool1   = keras.layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2))(relu1)
drop1   = keras.layers.Dropout(0.2)(pool1)

conv2   = keras.layers.Conv2D(64, (5, 5))(drop1)
relu2   = keras.layers.LeakyReLU(alpha=0.02)(conv2)
pool2   = keras.layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2))(relu2)
drop2   = keras.layers.Dropout(0.2)(pool2)

gmpool  = keras.layers.GlobalMaxPool2D()(drop2)

dense1  = keras.layers.Dense(1024)(gmpool)
relu3   = keras.layers.LeakyReLU(alpha=0.2)(dense1)
drop3   = keras.layers.Dropout(0.4)(relu3)

outputs = keras.layers.Dense(10, activation='softmax')(drop3)

model   = keras.Model(inputs=inputs, outputs=outputs)

model.compile(loss='sparse_categorical_crossentropy', optimizer='adam',
              metrics=['acc'])


print(model.summary())

filepath = "best1.hdf5"
checkpoint = keras.callbacks.ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=False, mode='max')

model.fit(trax, tray, epochs=10, callbacks=[checkpoint])
# model.fit(tex, tey, epochs=100)

model.save('model.h5')

