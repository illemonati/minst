
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt

minst = keras.datasets.mnist
(trax, tray), (tex, tey) = minst.load_data()
i = tex[0]
trax = np.array(trax / 255)
tex = np.array(tex / 255)
trax = np.expand_dims(trax, axis=3)
tex = np.expand_dims(tex, axis=3)


model = keras.models.load_model('best1.hdf5')
model.summary()

prediction = model.predict(np.array([tex[0]]))
print(prediction)
print(np.argmax(prediction))
plt.imshow(i)
plt.show()
